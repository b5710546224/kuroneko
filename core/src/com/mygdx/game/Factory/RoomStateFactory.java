package com.mygdx.game.Factory;

import com.mygdx.game.states.BathroomState;
import com.mygdx.game.states.BedroomState;
import com.mygdx.game.states.GameStateManager;
import com.mygdx.game.states.GameroomState;
import com.mygdx.game.states.KitchenState;
import com.mygdx.game.states.LivingroomState;
import com.mygdx.game.states.PlayState;
import com.mygdx.game.states.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 31/05/2016.
 */
public class RoomStateFactory {
    List<State> stateList;
    private GameStateManager gsm;
    public RoomStateFactory(GameStateManager gsm){
        stateList = new ArrayList<State>();
        this.gsm = gsm;
        createStateList();

    }
    public void createStateList(){
        stateList.add(new BedroomState(gsm));
        stateList.add(new BathroomState(gsm));
        stateList.add(new LivingroomState(gsm));
        stateList.add(new KitchenState(gsm));
        stateList.add(new GameroomState(gsm));
        stateList.add(new PlayState(gsm));
    }

    public State getState(String roomName) {
        if(roomName.equals("Bedroom")){
            return this.stateList.get(0);
        }else if(roomName.equals("Bathroom")){
            return this.stateList.get(1);
        }
        else if(roomName.equals("Livingroom")){
            return this.stateList.get(2);
        }else if(roomName.equals("Kitchen")){
            return this.stateList.get(3);
        }else if(roomName.equals("Gameroom")){
            return this.stateList.get(4);
        }else if(roomName.equals("play")){
            return this.stateList.get(5);
        }

        return null;
    }
}
