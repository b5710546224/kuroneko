package com.mygdx.game.Factory;

import com.mygdx.game.popup.EmotionState;
import com.mygdx.game.popup.PopUpState;
import com.mygdx.game.popup.PopUpStateManager;
import com.mygdx.game.popup.RefrigeratorState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 03/06/2016.
 */
public class PopUpStateFactory {
    List<PopUpState> stateList;
    private PopUpStateManager psm;
    public PopUpStateFactory(PopUpStateManager psm,int x,int y,int wid){
        stateList = new ArrayList<PopUpState>();
        this.psm = psm;
        createStateList(x,y,wid);

    }
    public void createStateList(int x,int y,int wid){
        stateList.add(new EmotionState(psm,x,y,wid));
        stateList.add(new RefrigeratorState(psm,x,y,wid));
    }

    public PopUpState getState(String stateName) {
        if(stateName.equals("emo")){
            return stateList.get(0);
        }else if(stateName.equals("ref")){
            return stateList.get(1);
        }
        return null;
    }
}