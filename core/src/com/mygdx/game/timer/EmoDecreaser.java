package com.mygdx.game.timer;


import com.mygdx.game.sprites.KuroNeko;

import java.util.TimerTask;


/**
 * Created by mind on 03/06/2016.
 */
public class EmoDecreaser extends TimerTask {

    int dps;

    public EmoDecreaser(int dps){
        this.dps = dps;
    }

    public void run(){
        KuroNeko.getInstance().minusEmo(dps);

    }

    public void setDps(int dps){
        this.dps = dps;
    }
}