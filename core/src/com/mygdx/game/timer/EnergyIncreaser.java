package com.mygdx.game.timer;

/**
 * Created by mind on 05/06/2016.
 */
import com.mygdx.game.sprites.KuroNeko;

import java.util.TimerTask;

/**
 * Created by mind on 05/06/2016.
 */
public class EnergyIncreaser extends TimerTask{
    int dps;

    public EnergyIncreaser(int dps){
        this.dps = dps;
    }

    public void run(){
        KuroNeko.getInstance().addEnergy(dps);
    }

    public void setDps(int dps){
        this.dps = dps;
    }
}
