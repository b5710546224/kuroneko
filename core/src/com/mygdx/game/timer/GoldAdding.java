package com.mygdx.game.timer;

import com.mygdx.game.sprites.KuroNeko;

import java.util.TimerTask;

/**
 * Created by mind on 05/06/2016.
 */
public class GoldAdding extends TimerTask {

    int dps;

    public GoldAdding(int dps){
        this.dps = dps;
    }



    public void setDps(int dps){
        this.dps = dps;
    }
    @Override
    public void run() {
        KuroNeko.getInstance().addingMinigameScore(1);
    }
}
