package com.mygdx.game.timer;

import com.mygdx.game.sprites.KuroNeko;

import java.util.TimerTask;

/**
 * Created by mind on 03/06/2016.
 */
public class EnergyDecreaser extends TimerTask{
    int dps;

    public EnergyDecreaser(int dps){
        this.dps = dps;
    }

    public void run(){
        KuroNeko.getInstance().minusEnergy(dps);
    }

    public void setDps(int dps){
        this.dps = dps;
    }
}
