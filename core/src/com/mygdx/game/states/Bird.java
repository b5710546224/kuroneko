package com.mygdx.game.states;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.KuroNekoApp;

/**
 * Created by mind on 05/06/2016.
 */
public class Bird{
    private static final int GRAVITY = -15;
    private static final int MOVEMENT = 100;
    private Vector3 position,velocity;
    private Texture bird;
    private Rectangle bounds;

    public Bird(int x,int y){
        position = new Vector3(x,y,0);
        velocity = new Vector3(0,0,0);
        bird = new Texture("bird.png");
        bounds = new Rectangle(x,y,bird.getWidth(),bird.getHeight());
    }
    public void update(float dt){
        if( position.y>0 ) velocity.add(0,GRAVITY,0);
        if(position.y >= KuroNekoApp.HEIGHT/2 - bird.getHeight()) position.y = KuroNekoApp.HEIGHT/2 - bird.getHeight() ;
        velocity.scl(dt);
        position.add(MOVEMENT*dt,velocity.y,0);
        if(position.y < 0) position.y = 0;

        velocity.scl(1/dt);
        bounds.setPosition(position.x,position.y);
    }
    public Vector3 getPosition(){return this.position;}
    public Texture getTexture(){return this.bird;}

    public void jump(){
        velocity.y = 250;
    }

    public boolean falled(){
        return position.y<=0;
    }

    public Rectangle getBounds(){
        return this.bounds;
    }

    public void dispose(){
        bird.dispose();
    }
}
