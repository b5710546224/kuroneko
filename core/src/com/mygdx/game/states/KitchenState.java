package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.KuroNekoApp;
import com.mygdx.game.popup.PopUpStateManager;
import com.mygdx.game.sprites.HealthBar;
import com.mygdx.game.sprites.KuroNeko;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 30/05/2016.
 */
public class KitchenState extends State {
    private Sprite bg,label,emo;
    private List<Sprite> buttonList;
    private List<Rectangle> boundsList;
    private int space,butSize;
    private float elapsedTime;
    private PopUpStateManager psm;
    private boolean emoClick,refClick;
    private Rectangle emoBound,refBound,okBound,cancelBound;;
    private int nekoX,nekoY,nekoWidth,nekoHeith,labelX,labelY,labelWidth,labelHeith;
    private int[][] numFood;
    private int[][] priceFood = {{20,25,10},{30,20,13},{40,35,50}};
    private int[][] hungryFood = {{2,4,1},{5,2,1},{6,5,9}};
    private int[][] emoFood = {{1,1,0},{2,1,1},{5,3,9}};
    private List<Sprite> addButList,minusButList;
    private List<Rectangle> addBoundsList,minusBoundList;
    private BitmapFont bitmapFont,moneyFont;
    private HealthBar emoDisplay,hungryDisplay;
    private Sprite coin;
    private int amountEmo,amoutHungry,amoutPrice;
    public KitchenState(GameStateManager gsm) {

        super(gsm);
        numFood = new int[3][3];

        emoClick = false;
        refClick = false;
        bitmapFont = new BitmapFont();
        bitmapFont.getData().setScale(3.5f,3.5f);

        moneyFont = new BitmapFont();
        moneyFont.getData().setScale(5.0f,5.0f);

        init();
        buttonList = new ArrayList<Sprite>();
        boundsList = new ArrayList<Rectangle>();

        buttonList.add(new Sprite(new Texture("bedroomBT.png")));
        buttonList.add(new Sprite(new Texture("bathroomBT.png")));
        buttonList.add(new Sprite(new Texture("livingroomBT.png")));
        buttonList.add(new Sprite(new Texture("kitchenBT2.png")));
        buttonList.add(new Sprite(new Texture("gameroomBT.png")));

        for(int i = 0;i<5;i++){
            buttonList.get(i).setSize(butSize,butSize);
            buttonList.get(i).setY(space);
            buttonList.get(i).setX((int)(space+(space+butSize)*i));
            boundsList.add(new Rectangle(buttonList.get(i).getX(),buttonList.get(i).getY(),butSize,butSize));
        }
        psm = new PopUpStateManager((int)bg.getX(),(int)bg.getY(),(int)bg.getWidth());
    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched()){
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            if(refClick){
                if(okBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    refClick = false;
                    KuroNeko.getInstance().addEmo(amountEmo);
                    KuroNeko.getInstance().addHungry(amoutHungry);
                    KuroNeko.getInstance().minusMoney(amoutPrice);
                    amoutPrice = 0;
                    numFood = new int[3][3];
                    psm.set("emo");

                }else if(cancelBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    refClick = false;
                    numFood = new int[3][3];
                    psm.set("emo");
                }else if(minusBoundList.get(0).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[0][0]-1 <=0){
                        numFood[0][0] = 0;
                    }else{
                        numFood[0][0] -= 1;
                    }
                }else if(minusBoundList.get(1).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[0][1]-1 <=0){
                        numFood[0][1] = 0;
                    }else{
                        numFood[0][1] -= 1;
                    }
                }else if(minusBoundList.get(2).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[0][2]-1 <=0){
                        numFood[0][2] = 0;
                    }else{
                        numFood[0][2] -= 1;
                    }
                }else if(minusBoundList.get(3).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[1][0]-1 <=0){
                        numFood[1][0] = 0;
                    }else{
                        numFood[1][0] -= 1;
                    }
                }else if(minusBoundList.get(4).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[1][1]-1 <=0){
                        numFood[1][1] = 0;
                    }else{
                        numFood[1][1] -= 1;
                    }
                }else if(minusBoundList.get(5).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[1][2]-1 <=0){
                        numFood[1][2] = 0;
                    }else{
                        numFood[1][2] -= 1;
                    }
                }else if(minusBoundList.get(6).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[2][0]-1 <=0){
                        numFood[2][0] = 0;
                    }else{
                        numFood[2][0] -= 1;
                    }
                }else if(minusBoundList.get(7).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[2][1]-1 <=0){
                        numFood[2][1] = 0;
                    }else{
                        numFood[2][1] -= 1;
                    }
                }else if(minusBoundList.get(8).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(numFood[2][2]-1 <=0){
                        numFood[2][2] = 0;
                    }else{
                        numFood[2][2] -= 1;
                    }
                }
                else if(addBoundsList.get(0).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[0][0]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[0][0] += 1;
                }else if(addBoundsList.get(1).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[0][1]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[0][1] += 1;
                }else if(addBoundsList.get(2).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[0][2]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[0][2] += 1;
                }else if(addBoundsList.get(3).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[1][0]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[1][0] += 1;
                }else if(addBoundsList.get(4).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[1][1]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[1][1] += 1;
                }else if(addBoundsList.get(5).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[1][2]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[1][2] += 1;
                }else if(addBoundsList.get(6).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[2][0]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[2][0] += 1;
                }else if(addBoundsList.get(7).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[2][1]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[2][1] += 1;
                }else if(addBoundsList.get(8).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    if(!(priceFood[2][2]+amoutPrice > KuroNeko.getInstance().getMoney()))
                        numFood[2][2] += 1;
                }

            }else{
                if(boundsList.get(0).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)) {
                    gsm.set("Bedroom");
                    emoClick = false;
                }else if(boundsList.get(1).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    gsm.set("Bathroom");
                    emoClick = false;
                }else if(boundsList.get(2).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    gsm.set("Livingroom");
                    emoClick = false;
                }else if(boundsList.get(4).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    gsm.set("Gameroom");
                    emoClick = false;
                }else if(emoBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    psm.set("emo");
                    emoClick = !emoClick;
                }else if(refBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                    psm.set("ref");
                    refClick = true;
                    emoClick = false;
                }
            }
        }

    }

    @Override
    public void update(float dt) {
        cam.setToOrtho(false, KuroNekoApp.WIDTH,KuroNekoApp.HEIGHT);
        handleInput();
        psm.update(dt);
        calculateAmout();
        emoDisplay.setHealth(KuroNeko.getInstance().getEmo()+amountEmo);
        hungryDisplay.setHealth(KuroNeko.getInstance().getHungry()+amoutHungry);
    }

    public void calculateAmout(){
        amountEmo = 0;
        amoutHungry = 0;
        amoutPrice = 0;
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                amountEmo+=numFood[i][j]*emoFood[i][j];
                amoutHungry+=numFood[i][j]*hungryFood[i][j];
                amoutPrice+=numFood[i][j]*priceFood[i][j];
            }
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        Gdx.gl.glClearColor(125/255.0f,77/255.0f,37/255.0f,1);
        sb.begin();
        sb.draw(bg,0, (int)((KuroNekoApp.HEIGHT/2.0)-(KuroNekoApp.WIDTH/2.0)),
                KuroNekoApp.WIDTH,KuroNekoApp.WIDTH);
        elapsedTime+= Gdx.graphics.getDeltaTime();
        sb.draw(KuroNeko.getInstance().getAnimation().getKeyFrame(elapsedTime,true),
                this.nekoX,this.nekoY,this.nekoWidth,this.nekoHeith);
        for(int i = 0;i<5;i++){
            buttonList.get(i).draw(sb);
        }
        label.draw(sb);
        emo.draw(sb);
        if(emoClick||refClick){
            psm.render(sb);
        }
        if(refClick){
            emoDisplay.render(sb);
            hungryDisplay.render(sb);
            bitmapFont.draw(sb,"Amount : "+amoutPrice,coin.getX(),(int)(coin.getY()+bg.getHeight()*0.12));
            for(int i = 0;i<3;i++){
                for(int j = 0;j<3;j++){
                    bitmapFont.draw(sb, numFood[i][j]+"",addButList.get(j+(i*3)).getX(), addButList.get(j+(i*3)).getY()+(int)(bg.getHeight()*0.15));
                    bitmapFont.draw(sb, numFood[i][j]*priceFood[i][j]+"",addButList.get(j+(i*3)).getX(), addButList.get(j+(i*3)).getY()+(int)(bg.getHeight()*0.1));
                }
            }
        }
        coin.draw(sb);
        moneyFont.draw(sb, KuroNeko.getInstance().getMoney()+"",(coin.getX()+coin.getWidth())+(int)(bg.getWidth()*0.03),coin.getY()+(int)(coin.getHeight()/2.0)+(int)(coin.getHeight()/3.0));
        sb.end();
    }

    @Override
    public void dispose() {

    }
    @Override
    public void init() {
        bg = new Sprite(new Texture("kitchenBG.png"));
        bg.setSize(KuroNekoApp.WIDTH,KuroNekoApp.WIDTH);
        bg.setX(0);
        bg.setY((int)((KuroNekoApp.HEIGHT/2.0)-(KuroNekoApp.WIDTH/2.0)));

        label = new Sprite(new Texture("kitchenLabel.png"));
        double persentLabel = (((KuroNekoApp.WIDTH*0.4))/label.getTexture().getWidth());
        label.setSize((int)(label.getTexture().getWidth()*persentLabel),(int)(label.getTexture().getHeight()*persentLabel));
        label.setX((KuroNekoApp.WIDTH/2)-(label.getWidth()/2));
        label.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emo = new Sprite(new Texture("emotion.png"));
        emo.setSize(label.getHeight(),label.getHeight());
        emo.setX((label.getX()/2)-(emo.getWidth()/2));
        emo.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emoBound = new Rectangle(emo.getX(),emo.getY(),emo.getWidth(),emo.getHeight());
        refBound = new Rectangle(bg.getX()+(int)(bg.getHeight()*0.03),bg.getY()+(int)(bg.getHeight()*0.14),
                (int)(bg.getHeight()*0.2),(int)(bg.getHeight()*0.39));

        okBound = new Rectangle((int)(bg.getWidth()*0.13),(int)(bg.getY()-((bg.getHeight()*0.2)/4.0)-(bg.getHeight()*0.2)/2),
                (int)(bg.getWidth()*0.34),(int)((bg.getHeight()*0.2)/2.0));
        cancelBound = new Rectangle((int)(bg.getWidth()*0.53),(int)(bg.getY()-((bg.getHeight()*0.2)/4.0)-(bg.getHeight()*0.2)/2),
                (int)(bg.getWidth()*0.34),(int)((bg.getHeight()*0.2)/2.0));


        this.nekoX = (int)((KuroNekoApp.WIDTH/2.0)-(KuroNekoApp.WIDTH*0.2));
        this.nekoY = (int)(bg.getY());
        this.nekoWidth = KuroNeko.getInstance().getWidth();
        this.nekoHeith = KuroNeko.getInstance().getWidth();
        space = (int)(KuroNekoApp.WIDTH*0.05);
        butSize = (int)(int)(KuroNekoApp.WIDTH*0.14);

        addButList = new ArrayList<Sprite>();
        minusButList = new ArrayList<Sprite>();
        addBoundsList = new ArrayList<Rectangle>();
        minusBoundList = new ArrayList<Rectangle>();
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                addButList.add(new Sprite(new Texture("add.png")));
                minusButList.add(new Sprite(new Texture("minus.png")));

                addButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                minusButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));

                addButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                addButList.get(j+(i*3)).setX(bg.getX()+(int)(bg.getWidth()*(0.28+(j*0.32)))-addButList.get(0).getWidth());
                addButList.get(j+(i*3)).setY(bg.getY()+(int)(bg.getWidth()*(0.67-(i*0.32))));
                addBoundsList.add(new Rectangle(addButList.get(j+(i*3)).getX(),addButList.get(j+(i*3)).getY(),
                        addButList.get(j+(i*3)).getWidth(),addButList.get(j+(i*3)).getHeight()));

                minusButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                minusButList.get(j+(i*3)).setX(bg.getX()+(int)(bg.getWidth()*(0.28+(j*0.32))));
                minusButList.get(j+(i*3)).setY(bg.getY()+(int)(bg.getWidth()*(0.67-(i*0.32))));
                minusBoundList.add(new Rectangle(minusButList.get(j+(i*3)).getX(),minusButList.get(j+(i*3)).getY(),
                        minusButList.get(j+(i*3)).getWidth(),minusButList.get(j+(i*3)).getHeight()));
            }
        }

        coin = new Sprite(new Texture("coin.png"));
        coin.setSize(emo.getWidth()/2,emo.getHeight()/2);
        coin.setY(emo.getY()+(int)(emo.getHeight()/2.0)-(int)(coin.getHeight()/2.0));
        coin.setX(label.getX()+label.getWidth()+(int)(bg.getWidth()*0.03));

        Texture a = new Texture("hpBG.png");
        Texture b = new Texture("hpFG.png");
        hungryDisplay = new HealthBar(a,b,KuroNeko.getInstance().getEmo(),
                (int)(bg.getWidth()*0.14),(int)(bg.getY()+bg.getHeight()+(int)((bg.getHeight()*0.2)*0.5)-((int)(bg.getHeight()*0.03/2))),
                (int)(bg.getWidth()*0.215),(int)(bg.getHeight()*0.03));
        emoDisplay = new HealthBar(a,b,KuroNeko.getInstance().getEmo(),
                (int)(bg.getWidth()*0.48),(int)(bg.getY()+bg.getHeight()+(int)((bg.getHeight()*0.2)*0.5)-((int)(bg.getHeight()*0.03/2))),
                (int)(bg.getWidth()*0.215),(int)(bg.getHeight()*0.03));

    }

}
