package com.mygdx.game.states;

/**
 * Created by mind on 05/06/2016.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.KuroNekoApp;
import com.mygdx.game.sprites.KuroNeko;
import com.mygdx.game.timer.GoldAdding;

import java.util.Timer;

/**
 * Created by mind on 17/04/2016.
 */
public class PlayState extends State {
    private static final int TUBE_SPACING = 125;
    private static final int TUBE_COUNT = 4;

    private Bird bird;
    private Texture bg;
    private Tube tube;
    private int money;
    private Array<Tube> tubes;
    private BitmapFont bitmapFont;
    private Timer timer;
    public PlayState(GameStateManager gsm){
        super(gsm);
        init();
    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched()){
            bird.jump();
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        bird.update(dt);
        cam.position.x = bird.getPosition().x + 80;

        for(Tube tube : tubes){
            if(cam.position.x - (cam.viewportWidth/2) > tube.getPosTopTube().x+tube.getTopTube().getWidth()){
                tube.reposition(tube.getPosTopTube().x + ((Tube.TUBE_WIDTH+TUBE_SPACING)*TUBE_COUNT));
            }
            if(tube.collides(bird.getBounds())||bird.falled()){
                cam.setToOrtho(false, KuroNekoApp.WIDTH,KuroNekoApp.HEIGHT);
                timer.cancel();
                KuroNeko.getInstance().collectMoney();
                KuroNeko.getInstance().resetMinigameScore();
                gsm.set("Bedroom");
            }
        }

        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg,cam.position.x-(cam.viewportWidth/2),0);
        sb.draw(bird.getTexture(),bird.getPosition().x,bird.getPosition().y);
        for(Tube tube : tubes) {
            sb.draw(tube.getTopTube(), tube.getPosTopTube().x, tube.getPosTopTube().y);
            sb.draw(tube.getBottomTube(), tube.getPosBotTube().x, tube.getPosBotTube().y);
        }
        bitmapFont.draw(sb, KuroNeko.getInstance().getMiniGameScore()+"",cam.position.x,cam.position.y);
        sb.end();
    }

    @Override
    public void dispose() {
        bg.dispose();
        bird.dispose();
        for(Tube tube : tubes){
            tube.dispose();
        }
    }

    @Override
    public void init() {
        money = 0;
        timer = new Timer();
        timer.scheduleAtFixedRate(new GoldAdding(1),0,4000);
        bitmapFont = new BitmapFont();
        bitmapFont.getData().setScale(3.5f,3.5f);
        bird = new Bird(50,300);
        bg = new Texture("bg.png");
        cam.setToOrtho(false, KuroNekoApp.WIDTH/(KuroNekoApp.WIDTH/bg.getWidth()),KuroNekoApp.HEIGHT/(KuroNekoApp.HEIGHT/bg.getHeight()));
        double a = (bg.getWidth()*100)/Gdx.graphics.getWidth();
        tube = new Tube(100);

        tubes = new Array<Tube>();
        for(int i = 1;i<=TUBE_COUNT;i++){
            tubes.add(new Tube(i*(TUBE_SPACING+Tube.TUBE_WIDTH)));
        }
    }
}