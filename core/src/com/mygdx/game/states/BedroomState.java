package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.KuroNekoApp;
import com.mygdx.game.popup.PopUpStateManager;
import com.mygdx.game.sprites.KuroNeko;
import com.mygdx.game.timer.EnergyIncreaser;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

/**
 * Created by mind on 29/05/2016.
 */
public class BedroomState extends State {
    private Sprite bg,label,emo,lamp;
    private Texture h1,h2,dot,m1,m2,dark;
    private List<Sprite> buttonList;
    private List<Rectangle> boundsList;
    private int space,butSize;
    private float elapsedTime;
    private PopUpStateManager psm;
    private boolean emoClick;
    private Rectangle emoBound,lampBound;
    private boolean isDark;
    private Timer timer;
    private EnergyIncreaser increaseEnergy;

    //    private Date date;
//    private String time,h,m,nh,nm;
    private int nekoX,nekoY,nekoWidth,nekoHeith,labelX,labelY,labelWidth,labelHeith;
    private BitmapFont moneyFont;
    private Sprite coin;

    public BedroomState(GameStateManager gsm) {

        super(gsm);
        isDark = false;
        emoClick = false;
        moneyFont = new BitmapFont();
        moneyFont.getData().setScale(5.0f,5.0f);
        init();


    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched()){
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            if(lampBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                isDark = !isDark;
                KuroNeko.getInstance().setSleep(isDark);
                if(isDark){
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new EnergyIncreaser(1),0,1000);
                    gsm.cancelEnergyDecrease();
                }else{
                    timer.cancel();
                    gsm.resumeEnergyDecrease();
                }
            }else if(boundsList.get(1).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                isDark = false;
                timer.cancel();
                gsm.resumeEnergyDecrease();
                KuroNeko.getInstance().setSleep(isDark);
                gsm.set("Bathroom");
                emoClick = false;
            }else if(boundsList.get(2).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                isDark = false;
                timer.cancel();
                gsm.resumeEnergyDecrease();
                KuroNeko.getInstance().setSleep(isDark);
                gsm.set("Livingroom");
                emoClick = false;
            }else if(boundsList.get(3).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                isDark = false;
                timer.cancel();
                gsm.resumeEnergyDecrease();
                KuroNeko.getInstance().setSleep(isDark);
                gsm.set("Kitchen");
                emoClick = false;
            }else if(boundsList.get(4).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                isDark = false;
                timer.cancel();
                gsm.resumeEnergyDecrease();
                KuroNeko.getInstance().setSleep(isDark);
                gsm.set("Gameroom");
                emoClick = false;
            }else if(emoBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                emoClick = !emoClick;
            }
        }

    }

    @Override
    public void update(float dt) {
        cam.setToOrtho(false, KuroNekoApp.WIDTH,KuroNekoApp.HEIGHT);
        handleInput();
        psm.update(dt);
//        time = "";
//        nm = ""+date.getMinutes();
//        nh = ""+date.getHours();
//        if(!nh.equals(h)){
//
//        }
//        if(h.length()<2){
//            time+="0";
//        }
//            time+=h+":";
//        if(m.length()<2){
//            time+="0";
//        }
//            time+=m;

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        Gdx.gl.glClearColor(124/255.0f,116/255.0f,105/255.0f,1);
        sb.begin();
        bg.draw(sb);
        elapsedTime+= Gdx.graphics.getDeltaTime();
        sb.draw(KuroNeko.getInstance().getAnimation().getKeyFrame(elapsedTime,true),
                this.nekoX,this.nekoY,this.nekoWidth,this.nekoHeith);
        if(isDark){
            sb.draw(dark,0,0,KuroNekoApp.WIDTH,KuroNekoApp.HEIGHT);
        }
        for(int i = 0;i<5;i++){
            buttonList.get(i).draw(sb);
        }
        lamp.draw(sb);
        label.draw(sb);
        emo.draw(sb);
        if(emoClick){
            psm.render(sb);
        }
        coin.draw(sb);
        moneyFont.draw(sb, KuroNeko.getInstance().getMoney()+"",(coin.getX()+coin.getWidth())+(int)(bg.getWidth()*0.03),coin.getY()+(int)(coin.getHeight()/2.0)+(int)(coin.getHeight()/3.0));

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void init() {

//        time = "";
//        m = ""+date.getMinutes();
//        h = ""+date.getHours();
//        if(h.length()<2){
//            time+="0";
//        }
//        time+=h+":";
//        if(m.length()<2){
//            time+="0";
//        }
//        time+=m;
//
//        h1 = new Texture(time.charAt(0)+".png");
//        h2 = new Texture(time.charAt(1)+".png");
//        dot = new Texture("dot.png");
//        m1 = new Texture(time.charAt(3)+".png");
//        m2 = new Texture(time.charAt(4)+".png");

//        date = new Date();
        bg = new Sprite(new Texture("bedroomBG.png"));
        bg.setSize(KuroNekoApp.WIDTH,KuroNekoApp.WIDTH);
        bg.setX(0);
        bg.setY((int)((KuroNekoApp.HEIGHT/2.0)-(KuroNekoApp.WIDTH/2.0)));

        label = new Sprite(new Texture("bedroomLabel.png"));
        double persentLabel = (((KuroNekoApp.WIDTH*0.4))/label.getTexture().getWidth());
        label.setSize((int)(label.getTexture().getWidth()*persentLabel),(int)(label.getTexture().getHeight()*persentLabel));
        label.setX((KuroNekoApp.WIDTH/2)-(label.getWidth()/2));
        label.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emo = new Sprite(new Texture("emotion.png"));
        emo.setSize(label.getHeight(),label.getHeight());
        emo.setX((label.getX()/2)-(emo.getWidth()/2));
        emo.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emoBound = new Rectangle(emo.getX(),emo.getY(),emo.getWidth(),emo.getHeight());

        this.nekoX = (int)((KuroNekoApp.WIDTH/2.0)-((KuroNeko.getInstance().getWidth()/2)/2.0));
        this.nekoY = (int)(bg.getY()+(bg.getHeight()*0.3));
        this.nekoWidth = KuroNeko.getInstance().getWidth()/2;
        this.nekoHeith = KuroNeko.getInstance().getHeight()/2;
        space = (int)(KuroNekoApp.WIDTH*0.05);
        butSize = (int)(int)(KuroNekoApp.WIDTH*0.14);

        coin = new Sprite(new Texture("coin.png"));
        coin.setSize(emo.getWidth()/2,emo.getHeight()/2);
        coin.setY(emo.getY()+(int)(emo.getHeight()/2.0)-(int)(coin.getHeight()/2.0));
        coin.setX(label.getX()+label.getWidth()+(int)(bg.getWidth()*0.03));

        buttonList = new ArrayList<Sprite>();
        boundsList = new ArrayList<Rectangle>();

        buttonList.add(new Sprite(new Texture("bedroomBT2.png")));
        buttonList.add(new Sprite(new Texture("bathroomBT.png")));
        buttonList.add(new Sprite(new Texture("livingroomBT.png")));
        buttonList.add(new Sprite(new Texture("kitchenBT.png")));
        buttonList.add(new Sprite(new Texture("gameroomBT.png")));

        for(int i = 0;i<5;i++){
            buttonList.get(i).setSize(butSize,butSize);
            buttonList.get(i).setY(space);
            buttonList.get(i).setX((int)(space+(space+butSize)*i));
            boundsList.add(new Rectangle(buttonList.get(i).getX(),buttonList.get(i).getY(),butSize,butSize));
        }
        psm = new PopUpStateManager((int)bg.getX(),(int)bg.getY(),(int)bg.getWidth());

        lamp = new Sprite(new Texture("lamp.png"));
        dark = new Texture("dark.png");
        lamp.setSize(butSize,butSize);
        lamp.setX((int)((KuroNekoApp.WIDTH/2.0) - (lamp.getWidth()/2.0)));
        lamp.setY(space+butSize+space);
        timer = new Timer();
        lampBound = new Rectangle(lamp.getX(),lamp.getY(),butSize,butSize);
    }
}
