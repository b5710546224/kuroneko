package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.KuroNekoApp;
import com.mygdx.game.popup.PopUpStateManager;
import com.mygdx.game.sprites.KuroNeko;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 30/05/2016.
 */
public class LivingroomState extends State{
    private Sprite bg,label,emo;
    private List<Sprite> buttonList;
    private List<Rectangle> boundsList;
    private int space,butSize;
    private float elapsedTime;
    private PopUpStateManager psm;
    private boolean emoClick;
    private Rectangle emoBound;
    private int nekoX,nekoY,nekoWidth,nekoHeith,labelX,labelY,labelWidth,labelHeith;
    private BitmapFont moneyFont;
    private Sprite coin;

    public LivingroomState(GameStateManager gsm) {

        super(gsm);
        moneyFont = new BitmapFont();
        moneyFont.getData().setScale(5.0f,5.0f);
        emoClick = false;
        init();
        buttonList = new ArrayList<Sprite>();
        boundsList = new ArrayList<Rectangle>();

        buttonList.add(new Sprite(new Texture("bedroomBT.png")));
        buttonList.add(new Sprite(new Texture("bathroomBT.png")));
        buttonList.add(new Sprite(new Texture("livingroomBT2.png")));
        buttonList.add(new Sprite(new Texture("kitchenBT.png")));
        buttonList.add(new Sprite(new Texture("gameroomBT.png")));

        for(int i = 0;i<5;i++){
            buttonList.get(i).setSize(butSize,butSize);
            buttonList.get(i).setY(space);
            buttonList.get(i).setX((int)(space+(space+butSize)*i));
            boundsList.add(new Rectangle(buttonList.get(i).getX(),buttonList.get(i).getY(),butSize,butSize));
        }
        psm = new PopUpStateManager((int)bg.getX(),(int)bg.getY(),(int)bg.getWidth());
    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched()){
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            if(boundsList.get(0).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                gsm.set("Bedroom");
                emoClick = false;
            }else if(boundsList.get(1).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                gsm.set("Bathroom");
                emoClick = false;
            }else if(boundsList.get(3).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                gsm.set("Kitchen");
                emoClick = false;
            }else if(boundsList.get(4).contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                gsm.set("Gameroom");
                emoClick = false;
            }else if(emoBound.contains(touchPos.x,KuroNekoApp.HEIGHT-touchPos.y)){
                emoClick = !emoClick;
            }
        }

    }

    @Override
    public void update(float dt) {
        cam.setToOrtho(false, KuroNekoApp.WIDTH,KuroNekoApp.HEIGHT);
        handleInput();
        psm.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        Gdx.gl.glClearColor(191/255.0f,169/255.0f,145/255.0f,1);
        sb.begin();
        sb.draw(bg,0, (int)((KuroNekoApp.HEIGHT/2.0)-(KuroNekoApp.WIDTH/2.0)),
                KuroNekoApp.WIDTH,KuroNekoApp.WIDTH);
        elapsedTime+= Gdx.graphics.getDeltaTime();
        sb.draw(KuroNeko.getInstance().getAnimation().getKeyFrame(elapsedTime,true),
                this.nekoX,this.nekoY,this.nekoWidth,this.nekoHeith);
        for(int i = 0;i<5;i++){
            buttonList.get(i).draw(sb);
        }
        emo.draw(sb);
        label.draw(sb);
        if(emoClick){
            psm.render(sb);
        }
        coin.draw(sb);
        moneyFont.draw(sb, KuroNeko.getInstance().getMoney()+"",(coin.getX()+coin.getWidth())+(int)(bg.getWidth()*0.03),coin.getY()+(int)(coin.getHeight()/2.0)+(int)(coin.getHeight()/3.0));

        sb.end();
    }

    @Override
    public void dispose() {

    }
    @Override
    public void init() {
        bg = new Sprite(new Texture("livingroomBG.png"));
        bg.setSize(KuroNekoApp.WIDTH,KuroNekoApp.WIDTH);
        bg.setX(0);
        bg.setY((int)((KuroNekoApp.HEIGHT/2.0)-(KuroNekoApp.WIDTH/2.0)));

        label = new Sprite(new Texture("livingroomLabel.png"));
        double persentLabel = (((KuroNekoApp.WIDTH*0.4))/label.getTexture().getWidth());
        label.setSize((int)(label.getTexture().getWidth()*persentLabel),(int)(label.getTexture().getHeight()*persentLabel));
        label.setX((KuroNekoApp.WIDTH/2)-(label.getWidth()/2));
        label.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emo = new Sprite(new Texture("emotion.png"));
        emo.setSize(label.getHeight(),label.getHeight());
        emo.setX((label.getX()/2)-(emo.getWidth()/2));
        emo.setY((bg.getY()+bg.getWidth())+(int)(bg.getHeight()*0.02));

        emoBound = new Rectangle(emo.getX(),emo.getY(),emo.getWidth(),emo.getHeight());

        this.nekoX = (int)((KuroNekoApp.WIDTH/2.0)-((KuroNeko.getInstance().getWidth()/1.5)/2));
        this.nekoY = (int)(bg.getY()+(bg.getHeight()*0.33));
        this.nekoWidth = (int)(KuroNeko.getInstance().getWidth()/1.5);
        this.nekoHeith = (int)(KuroNeko.getInstance().getWidth()/1.5);
        space = (int)(KuroNekoApp.WIDTH*0.05);
        butSize = (int)(int)(KuroNekoApp.WIDTH*0.14);

        coin = new Sprite(new Texture("coin.png"));
        coin.setSize(emo.getWidth()/2,emo.getHeight()/2);
        coin.setY(emo.getY()+(int)(emo.getHeight()/2.0)-(int)(coin.getHeight()/2.0));
        coin.setX(label.getX()+label.getWidth()+(int)(bg.getWidth()*0.03));

    }
}
