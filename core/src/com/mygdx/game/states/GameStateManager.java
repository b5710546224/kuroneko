package com.mygdx.game.states;
/**
 * Created by mind on 29/05/2016.
 */
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Factory.RoomStateFactory;
import com.mygdx.game.sprites.KuroNeko;
import com.mygdx.game.timer.DirtyDecreaser;
import com.mygdx.game.timer.EmoDecreaser;
import com.mygdx.game.timer.EnergyDecreaser;
import com.mygdx.game.timer.HungryDecreaser;

import java.util.Stack;
import java.util.Timer;

public class GameStateManager {
    private Stack<State> states;
    private RoomStateFactory roomFactory;
    private EmoDecreaser timer1;
    private DirtyDecreaser timer2;
    private EnergyDecreaser timer3;
    private HungryDecreaser timer4;
    private Timer timer ,timerEnergy;
    public GameStateManager(){
        states = new Stack<State>();
        roomFactory = new RoomStateFactory(this);
        states.push(roomFactory.getState("Bedroom"));
        timer1 = new EmoDecreaser(1);
        timer2  = new DirtyDecreaser(1);
        timer3  = new EnergyDecreaser(1);
        timer4 = new HungryDecreaser(1);
        timer = new Timer();
        timerEnergy = new Timer();
        timer.scheduleAtFixedRate(timer1,0,1000);
        timer.scheduleAtFixedRate(timer2,0,1000);
        timerEnergy.scheduleAtFixedRate(timer3,0,1000);
        timer.scheduleAtFixedRate(timer4,0,1000);
    }
    public void cancelEnergyDecrease(){
        timerEnergy.cancel();
        System.out.println("cancel in");
    }
    public void resumeEnergyDecrease(){
        timerEnergy = new Timer();
        timerEnergy.scheduleAtFixedRate(new EnergyDecreaser(1),0,1000);
        System.out.println("resume in");

    }

    public void push(State state){states.push(state);}

    public void pop(){states.pop().dispose();}

    public void setToPlayState(){
        states.pop();
        states.push(new PlayState(this));
    }
    public void set(String roomName){
        states.pop();
        states.push(roomFactory.getState(roomName));
    }

    public void update(float dt){
        states.peek().update(dt);
        KuroNeko.getInstance().update();
    }

    public void render(SpriteBatch sb){states.peek().render(sb);}
}
