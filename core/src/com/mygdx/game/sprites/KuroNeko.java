package com.mygdx.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.KuroNekoApp;

/**
 * Created by mind on 30/05/2016.
 */
public class KuroNeko {
    private Texture catFrame,catFrameSad,catFrameSleep;
    private TextureRegion[] animationFrames;
    private Animation currentAnimation,happyAnimation,sadAnimation,sleepAnimation;
    private int persent;
    private int emo,hungry,energy,dirty;
    private Rectangle bounds;
    private float elapsedTime;
    TextureRegion[][] tmpFrames;
    private static KuroNeko instance;
    private int money;
    private boolean sleep;
    private int miniGameScore;

    private KuroNeko(){

        sleep = false;
        catFrame = new Texture("catFrame.png");
        catFrameSad = new Texture("catFrameSad.png");
        catFrameSleep = new Texture("catFrameSleep.png");
        this.money = 100;
        this.emo = 100;
        this.hungry = 100;
        this.energy = 100;
        this.dirty = 100;

        tmpFrames = TextureRegion.split(catFrame,500,500);

        animationFrames = new TextureRegion[4];
        int index =0;

        for(int i = 0;i<2;i++){
            for(int j = 0;j<2;j++){
                animationFrames[index++] = tmpFrames[j][i];
            }
        }

        happyAnimation = new Animation(1f/2f,animationFrames);
        currentAnimation = happyAnimation;

        tmpFrames = TextureRegion.split(catFrameSad,500,500);

        animationFrames = new TextureRegion[4];
        index =0;

        for(int i = 0;i<2;i++){
            for(int j = 0;j<2;j++){
                animationFrames[index++] = tmpFrames[j][i];
            }
        }
        sadAnimation = new Animation(1f/2f,animationFrames);

        tmpFrames = TextureRegion.split(catFrameSleep,500,500);

        animationFrames = new TextureRegion[4];
        index =0;

        for(int i = 0;i<2;i++){
            for(int j = 0;j<2;j++){
                animationFrames[index++] = tmpFrames[j][i];
            }
        }
        sleepAnimation = new Animation(1f/2f,animationFrames);



        persent = (int)(((KuroNekoApp.WIDTH*0.6)*100)/tmpFrames[0][0].getRegionWidth());
    }

    public static KuroNeko getInstance(){
        if(instance == null){
            return instance = new KuroNeko();
        }else{
            return instance;
        }
    }
    public int getMoney(){return this.money;}
    public int getEmo(){return this.emo;}
    public int getHungry(){return this.hungry;}
    public int getEnergy(){
        return this.energy;
    }
    public void minusEmo(int n){
        if(this.emo-n<=0){this.emo = 0;}
        else{this.emo-=n;}
    }
    public void addingMinigameScore(int a ){
        this.miniGameScore+=a;
    }
    public void resetMinigameScore(){
        this.miniGameScore=0;
    }
    public void collectMoney(){
        this.money+=miniGameScore;
    }
    public int getMiniGameScore(){
        return this.miniGameScore;
    }
    public void addMoney(int n){
       this.money+=n;
    }

    public void setToSad(){
        this.currentAnimation = sadAnimation;
    }
    public void setToHappy(){
        this.currentAnimation = happyAnimation;
    }
    public void setToSleep(){
        this.currentAnimation = sleepAnimation;
    }

    public void setSleep(boolean isSleep){
        this.sleep = isSleep;
    }
    public boolean getSleep(){
        return this.sleep;
    }

    public void minusMoney(int n){
        if(this.money-n<=0){this.money = 0;}
        else{this.money-=n;}
    }

    public void addEmo(int n){
        if(this.emo+n >= 100){this.emo = 100;}
        else{this.emo+=n;}
    }

    public void minusHungry(int n){
        if(this.hungry-n<=0){this.hungry = 0;}
        else{this.hungry-=n;}
    }
    public void addHungry(int n){
        if(this.hungry+n >= 100){this.hungry = 100;}
        else{this.hungry+=n;}
    }

    public void minusDirty(int n){
        if(this.dirty-n<=0){this.dirty = 0;}
        else{this.dirty-=n;}
    }
    public void addDirty(int n){
        if(this.dirty+n >= 100){this.dirty = 100;}
        else{this.dirty+=n;}
    }

    public void minusEnergy(int n){
        if(this.energy-n<=0){this.energy = 0;}
        else{this.energy-=n;}
    }
    public void addEnergy(int n){
        if(this.energy+n >= 100){this.energy = 100;}
        else{this.energy+=n;}
    }


    public int getDirty(){return this.dirty;}
    public int getWidth(){
        return (int)(tmpFrames[0][0].getRegionWidth()*(persent/100.0));
    }
    public int getHeight(){
        return (int)(tmpFrames[0][0].getRegionHeight()*(persent/100.0));
    }
    public Animation getAnimation(){return this.currentAnimation;}
    public void update(){
        if(sleep){
            setToSleep();
        }
        else if((emo+hungry+energy+dirty)/4.0 <=30){
            setToSad();
        }else {
            setToHappy();
        }
    }
}
