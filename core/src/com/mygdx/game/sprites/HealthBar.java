package com.mygdx.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by mind on 03/06/2016.
 */
public class HealthBar {
    private Sprite healthBarBG,healthBarFG;
    private int health;
    private int maxHealth;

    public HealthBar(Texture bg,Texture fg,int hp,int x,int y,int wid,int high){
        health = hp;
        maxHealth = 100;

        healthBarBG = new Sprite(bg);
        healthBarFG = new Sprite(fg);

        healthBarBG.setSize(wid,high);
        healthBarFG.setSize(wid,high);

        healthBarBG.setX(x);
        healthBarBG.setY(y);
        healthBarFG.setX(x);
        healthBarFG.setY(y);

        healthBarFG.setOrigin(0,0);
        healthBarFG.setScale(health/(float)maxHealth,1f);
    }
    public int getX(){
        return (int)this.healthBarBG.getX();
    }
    public int getY(){
        return (int)this.healthBarBG.getY();
    }
    public void setHP(int hp){
        this.health = hp;
        this.maxHealth = hp;
        healthBarFG.setScale(health/(float)maxHealth,1f);
    }
    public int getHP(){
        return this.health;
    }
    public void setHealth(int n){
        this.health = n;
        healthBarFG.setScale(health/(float)maxHealth,1f);
    }

    public void update(){

    }

    public void render(SpriteBatch batch){
        healthBarBG.draw(batch);
        healthBarFG.draw(batch);
    }
}
