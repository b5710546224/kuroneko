package com.mygdx.game.popup;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 03/06/2016.
 */
public class RefrigeratorState extends PopUpState{
    private Sprite bg,head,but;
    private List<Sprite> addButList,minusButList;
    private List<Rectangle> addBoundsList,minusBoundList;
    private Rectangle okBound,cancelBound;
    private double persent;
    public RefrigeratorState(PopUpStateManager psm,int x,int y,int wid) {
        super(psm);

        bg = new Sprite(new Texture("refrigeratorBG.png"));
        head = new Sprite(new Texture("refrigeratorHead.png"));
        but = new Sprite(new Texture("refrigeratorBut.png"));

        persent = ((double)(wid)/bg.getTexture().getWidth());

        bg.setSize(wid,wid);
        bg.setX(x);
        bg.setY(y);

        head.setSize(wid,(int)(persent*head.getTexture().getHeight()));
        head.setX(x);
        head.setY(bg.getY()+wid);

        but.setSize(wid,(int)(persent*but.getTexture().getHeight()));
        but.setX(x);
        but.setY(y-but.getHeight());

        addButList = new ArrayList<Sprite>();
        minusButList = new ArrayList<Sprite>();
        addBoundsList = new ArrayList<Rectangle>();
        minusBoundList = new ArrayList<Rectangle>();
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                addButList.add(new Sprite(new Texture("add.png")));
                minusButList.add(new Sprite(new Texture("minus.png")));

                addButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                minusButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));

                addButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                addButList.get(j+(i*3)).setX(x+(int)(bg.getWidth()*(0.28+(j*0.32)))-addButList.get(0).getWidth());
                addButList.get(j+(i*3)).setY(y+(int)(bg.getWidth()*(0.67-(i*0.32))));

                minusButList.get(j+(i*3)).setSize((int)(bg.getWidth()*0.05),(int)(bg.getWidth()*0.05));
                minusButList.get(j+(i*3)).setX(x+(int)(bg.getWidth()*(0.28+(j*0.32))));
                minusButList.get(j+(i*3)).setY(y+(int)(bg.getWidth()*(0.67-(i*0.32))));
            }
        }

        okBound = new Rectangle((int)(bg.getWidth()*0.13),(int)(bg.getY()-((bg.getHeight()*0.2)/4.0)-(bg.getHeight()*0.2)/2),
                (int)(bg.getWidth()*0.34),(int)((bg.getHeight()*0.2)/2.0));

        cancelBound = new Rectangle((int)(bg.getWidth()*0.53),(int)(bg.getY()-((bg.getHeight()*0.2)/4.0)-(bg.getHeight()*0.2)/2),
                (int)(bg.getWidth()*0.34),(int)((bg.getHeight()*0.2)/2.0));
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        bg.draw(sb);
        head.draw(sb);
        but.draw(sb);

        for(int i = 0;i<addButList.size();i++){
            addButList.get(i).draw(sb);
            minusButList.get(i).draw(sb);
        }
    }

    @Override
    public void dispose() {

    }
}
