package com.mygdx.game.popup;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.sprites.HealthBar;
import com.mygdx.game.sprites.KuroNeko;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 03/06/2016.
 */
public class EmotionState extends PopUpState {
    private List<HealthBar> emoList;
    private Sprite bg;
    private double persent;
    public EmotionState(PopUpStateManager psm,int x,int y,int wid) {

        super(psm);
        bg = new Sprite(new Texture("emotionPopup.png"));
        persent = ((wid/2.0)/bg.getTexture().getWidth());
        bg.setSize((int)(persent*bg.getTexture().getWidth()),(int)(persent*bg.getTexture().getHeight()));
        bg.setX(x);
        bg.setY((y+wid)-bg.getHeight());
        emoList = new ArrayList<HealthBar>();
        Texture a = new Texture("hpBG.png");
        Texture b = new Texture("hpFG.png");
        emoList.add(new HealthBar(a,b, KuroNeko.getInstance().getHungry(),
                (int)(x+(bg.getWidth()*0.35)),
                (int)(y+(wid*0.89)),
                (int)(bg.getWidth()*0.6),(int)(bg.getHeight()*0.05)));
        emoList.add(new HealthBar(a,b, KuroNeko.getInstance().getEmo(),
                (int)(x+(bg.getWidth()*0.35)),
                (int)(y+(wid*0.74)),
                (int)(bg.getWidth()*0.6),(int)(bg.getHeight()*0.05)));
        System.out.print(KuroNeko.getInstance().getEnergy()+" = my en");
        emoList.add(new HealthBar(a,b, KuroNeko.getInstance().getEnergy(),
                (int)(x+(bg.getWidth()*0.35)),
                (int)(y+(wid*0.59)),
                (int)(bg.getWidth()*0.6),(int)(bg.getHeight()*0.05)));
        emoList.add(new HealthBar(a,b, KuroNeko.getInstance().getDirty(),
                (int)(x+(bg.getWidth()*0.35)),
                (int)(y+(wid*0.44)),
                (int)(bg.getWidth()*0.6),(int)(bg.getHeight()*0.05)));


    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

        emoList.get(0).setHealth(KuroNeko.getInstance().getHungry());
        emoList.get(1).setHealth(KuroNeko.getInstance().getEmo());
        emoList.get(2).setHealth(KuroNeko.getInstance().getEnergy());
        emoList.get(3).setHealth(KuroNeko.getInstance().getDirty());

    }

    @Override
    public void render(SpriteBatch sb) {
        bg.draw(sb);
        for(int i = 0;i<emoList.size();i++)
            emoList.get(i).render(sb);
    }

    @Override
    public void dispose() {

    }
}
