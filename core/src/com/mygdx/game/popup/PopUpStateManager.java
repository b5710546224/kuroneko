package com.mygdx.game.popup;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Factory.PopUpStateFactory;

import java.util.Stack;

/**
 * Created by mind on 03/06/2016.
 */
public class PopUpStateManager {
    private Stack<PopUpState> states;
    PopUpStateFactory popupFactory;
    public PopUpStateManager(int x,int y,int wid){
        states = new Stack<PopUpState>();
        popupFactory = new PopUpStateFactory(this,x,y,wid);
        states.push(popupFactory.getState("emo"));
    }

    public void push(PopUpState state){states.push(state);}

    public void pop(){states.pop().dispose();}

    public void set(String state){
        states.pop();
        states.push(popupFactory.getState(state));
    }

    public void update(float dt){states.peek().update(dt);}

    public void render(SpriteBatch sb){states.peek().render(sb);}
}