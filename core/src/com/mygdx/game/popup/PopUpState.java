package com.mygdx.game.popup;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by mind on 03/06/2016.
 */
public abstract class PopUpState  {
    protected PopUpStateManager psm;
    public PopUpState(PopUpStateManager psm){
        this.psm = psm;

    }
    public  abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();
}